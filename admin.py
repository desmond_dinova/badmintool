import http.client
import hashlib
import json
import logging
import os
import time

servers = {
    "Local": {"url": "localhost", "port": 3010, "secret": "dSKwH5UGvCmkW8g9foyj"},
    "Alpha": {"url": "bs.plamee.com", "port": 80, "secret": "dSKwH5UGvCmkW8g9foyj"}
}


def set_logger(log_name: str, logs_path: str, write_logs: bool):
    if write_logs:
        if not os.path.exists(logs_path):
            os.mkdir(logs_path)

        logging.basicConfig(filename=os.path.join(logs_path, '{0}_{1}.log'.format(time.strftime(
                            '%Y_%m_%d_%H_%M', time.localtime()), log_name)), filemode='w')

    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s %(levelname)-9s| %(message)s'.format(log_name),
                        datefmt='%Y.%m.%d|%H:%M|')
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    formatter = logging.Formatter('%(funcName)-20s: %(levelname)-8s %(message)s')
    console.setFormatter(formatter)
    logging.getLogger('').addHandler(console)


class Admin:
    def __init__(self, server="Local", state_path="C:/BB_states/", logs_path="/logs/", write_logs=False):
        self.url = servers[server]["url"]
        self.port = servers[server]["port"]
        self.secret_key = servers[server]["secret"]
        self.state_path = state_path
        self.logs_path = logs_path
        self.logger = logging.getLogger(server)

        set_logger(server, self.logs_path, write_logs=write_logs)

    def get_state(self, user_id: int):
        hash_object = hashlib.md5(self.secret_key.encode('utf-8'))
        conn = http.client.HTTPConnection(self.url, self.port)

        headers = {"Content-Type": "application/json",
                   "x-sign-token": hash_object.hexdigest()}

        try:
            conn.request("POST", "/admin/support/users/{}".format(user_id), body=None, headers=headers)
        except:
            self.logger.critical("Cannot connect server: {0}".format(self.url))
            self.logger.info("Please, check, if this url is correct: http://{0}/{1}/{2}".format(self.url,
                                                                                              "admin/support/users",
                                                                                              user_id))
            conn.close()
            return None
        response = conn.getresponse()
        state = json.loads((response.read()).decode('utf-8'))

        if not state:
            self.logger.warning("There is no user {}".format(user_id))
            conn.close()
            return None
        if "error" in state and "message" in state:
            self.logger.error("{0} error occured: {1}".format(state["error"], state["message"]))
            #self.logger.info("Please, check, if your secret key is correct: {}".format(self.secret_key))
            conn.close()
            return None

        conn.close()
        return state["state"]

    def set_state(self, user_id: int, state: dict):
        conn = http.client.HTTPConnection(self.url, self.port)
        body = {
            "method": "state",
            "params": {
                "state": state
            }
        }
        body = json.dumps(body)

        hash_object = hashlib.md5((self.secret_key + body).encode('utf-8'))
        headers = {"Content-Type": "application/json",
                   "x-sign-token": hash_object.hexdigest()}

        try:
            conn.request("POST", "/admin/support/users/{}".format(user_id), body=body, headers=headers)
        except:
            self.logger.critical("Cannot connect server: {0}".format(self.url))
            self.logger.info("Please, check, if this url is correct: http://{0}/{1}/{2}".format(self.url,
                                                                                                "admin/support/users",
                                                                                                user_id))
            conn.close()
            return
        response = conn.getresponse()

        state = json.loads((response.read()).decode('utf-8'))

        if not state:
            self.logger.warning("There is no user {}".format(user_id))
            conn.close()
            return

        if "error" in state and "message" in state:
            self.logger.error("{0} error occured: {1}".format(state["error"], state["message"]))
            # self.logger.info("Please, check, if your secret key is correct: {}".format(self.secret_key))
            conn.close()
            return
        else:
            self.logger.info("State has been set to user {}".format(user_id))
            conn.close()

    def save_state(self, user_id: int, state_path="C:/BB_states/"):
        path_to_save = state_path if state_path != self.state_path else self.state_path

        if not os.path.exists(path_to_save):
            os.mkdir(path_to_save)

        state = self.get_state(user_id)
        
        json.dump(state, open(os.path.join(path_to_save, '{}.json'.format(str(user_id))), 'w'))

    def spawn_npc(self, user_id: int, npc_base: str, cell_id: int, is_hard=False, rewards=None):
        if not rewards:
            rewards = [
                {
                    "resources": {
                        "labProductWhite": 15
                    }
                },
                {
                    "resources": {
                        "bucks": 100000,
                        "bricks": 100,
                    }
                },
                {
                    "consumables": {
                        "journalDefense1": 1
                    }
                }
            ]

        state = self.get_state(user_id)
        cell_id = str(cell_id)
        player_map = state["city"]["openCells"]

        if cell_id not in player_map:
            self.logger.info("Cell with ID: {} is not opened".format(cell_id))
            self.logger.info("Adding cell {0} with NPC base {1} to state".format(cell_id, npc_base))
            state["city"]["openCells"].update({cell_id: {"state": {}}})

        cell = player_map[cell_id]["state"]

        if "own" in cell:
            self.logger.info("Cell with ID: {} is empty".format(cell_id))

        if "npc" in cell:
            self.logger.info("Cell with ID: {0} is occupied by NPC {1}".format(cell_id, cell["npc"]["id"]))

        if "enemy" in cell:
            self.logger.info("Cell with ID: {0} is occupied by player {1} base".format(cell_id, cell["enemy"]["id"]))

        self.logger.info("Spawning NPC base {0} in cell {1}".format(npc_base, cell_id))
        state["city"]["openCells"][cell_id]["state"].clear()
        state["city"]["openCells"][cell_id]["state"] = {
            "npc": {
                "id": npc_base,
                "isHard": is_hard,
                "rewards": rewards,
                "spawnTime": int(time.time() * 1000)
            }
        }

        self.set_state(user_id, state)

    def spawn_player(self, user_id: int, enemy_id: int, cell_id: int, rewards=None):
        if not rewards:
            rewards = [
                {
                    "resources": {
                        "labProductWhite": 79
                    }
                },
                {
                    "resources": {
                        "bucks": 100000,
                        "bricks": 100,
                    }
                },
                {
                    "consumables": {
                        "wantedPoster1": 1
                    }
                }
            ]

        state = self.get_state(user_id)
        cell_id = str(cell_id)
        player_map = state["city"]["openCells"]

        if cell_id not in player_map:
            self.logger.info("Cell with ID: {} is not opened".format(cell_id))
            self.logger.info("Adding cell {0} with player {1} base to state".format(cell_id, enemy_id))
            state["city"]["openCells"].update({cell_id: {"state": {}}})

        cell = player_map[cell_id]["state"]

        if "own" in cell:
            self.logger.info("Cell with ID: {} is empty".format(cell_id))

        if "npc" in cell:
            self.logger.info("Cell with ID: {0} is occupied by NPC {1}".format(cell_id, cell["npc"]["id"]))

        if "enemy" in cell:
            self.logger.info("Cell with ID: {0} is occupied by player {1} base".format(cell_id, cell["enemy"]["id"]))

        self.logger.info("Spawning player {0} base in cell {1}".format(cell_id, enemy_id))
        state["city"]["openCells"][cell_id]["state"].clear()
        state["city"]["openCells"][cell_id]["state"] = {
            "enemy": {
                "id": enemy_id,
                "rewards": rewards,
                "spawnTime": int(time.time() * 1000)
            }
        }
        self.set_state(user_id, state)

    def set_real(self, user_id: int, amount: int):
        state = self.get_state(user_id)
        state["player"]["resources"]["real"] = amount
        self.set_state(user_id, state)

    def set_bricks(self, user_id: int, amount: int):
        state = self.get_state(user_id)
        state["player"]["resources"]["bricks"] = amount
        self.set_state(user_id, state)

    def set_bucks(self, user_id: int, amount: int):
        state = self.get_state(user_id)
        state["player"]["resources"]["bucks"] = amount
        self.set_state(user_id, state)

    def set_name(self, user_id: int, name: str):
        state = self.get_state(user_id)
        state["player"]["name"] = name
        self.set_state(user_id, state)

    def set_exp(self, user_id: int, amount: int):
        state = self.get_state(user_id)
        state["player"]["exp"] = amount
        self.set_state(user_id, state)

    def set_rating(self, user_id: int, amount: int):
        state = self.get_state(user_id)
        state["player"]["rating"] = amount
        self.set_state(user_id, state)
